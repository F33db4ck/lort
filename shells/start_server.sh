#!/bin/bash

# Go to app folder
cd /home/app/lort

# Install missing dep
sudo npm i

# Copy .env
sudo cp ../envs/lort.env ./.env

# Restart app pm2
if ! sudo pm2 restart server.js ;
then
  sudo pm2 start server.js ;
else 
  sudo pm2 restart server.js ;
fi 
