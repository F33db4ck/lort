const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../../../server')
const expect = chai.expect

chai.use(chaiHttp)

describe('Equipment', () => {
  describe('/GET equipment', () => {
    it('it should GET all equipment', (done) => {
      chai.request(server)
        .get('/equipment')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.body.should.be.a('array')
          res.body.type.should.be.equal('OK')
          done()
        })
    })

    it('it should not GET a equipment', (done) => {
      chai.request(server)
        .get('/equipment/123e4567-e89b-12d3-a456-42661417400r')
        .end((err, res) => {
          expect(res.status).to.not.equal(200)
          res.body.should.be.a('object')
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          expect(res.body.body).to.be.null
          done()
        })
    })

    it('it should GET an equipment', (done) => {
      chai.request(server)
        .get('/equipment/123e4567-e89b-12d3-a456-42661417400e')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.body.should.be.a('object')
          res.body.type.should.be.equal('OK')
          done()
        })
    })

    it('it should GET all equipments with manufacturers', (done) => {
      chai.request(server)
        .get('/equipment/manufacturer')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.body.should.be.a('array')
          res.body.type.should.be.equal('OK')
          done()
        })
    })

    it('it should GET manufacturer of an equipment', (done) => {
      chai.request(server)
        .get('/equipment/123e4567-e89b-12d3-a456-42661417400e/manufacturer')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('OK')
          done()
        })
    })
  })

  describe('/POST equipment', () => {
    it('it should not POST a equipment', (done) => {
      chai.request(server)
        .post('/equipment')
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('BAD_REQUEST')
          done()
        })
    })

    it('it should POST a equipment with on body id', (done) => {
      const jsonData = {
        id: '123e4567-e89b-12d3-a456-42661417400f',
        manufacturerId: '123e4567-e89b-12d3-a456-426614174002',
        model: 'Oakenshield',
        serialNumber: '000001BC'
      }
      chai.request(server)
        .post('/equipment')
        .send(jsonData)
        .end((err, res) => {
          res.status.should.be.oneOf([201, 409])
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.oneOf(['CREATED', 'ALREADY_EXISTS'])
          if (res.status === 201) {
            expect(res.body.body).to.be.a('object')
          } else {
            expect(res.body.body).to.be.null
          }
          done()
        })
    })

    it('it should POST a equipment without id and manufacturerId on body', (done) => {
      const jsonData = {
        manufacturerId: '123e4567-e89b-12d3-a456-426614174002',
        model: 'Oakenshield 2',
        serialNumber: '000001BD'
      }
      chai.request(server)
        .post('/equipment')
        .send(jsonData)
        .end((err, res) => {
          res.status.should.be.oneOf([201, 409])
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.oneOf(['CREATED', 'ALREADY_EXISTS'])
          if (res.status === 201) {
            expect(res.body.body).to.be.a('object')
          } else {
            expect(res.body.body).to.be.null
          }
          done()
        })
    })
  })

  describe('/PUT manufacturer', () => {
    it('it should not PUT a equipment', (done) => {
      chai.request(server)
        .put('/equipment')
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('BAD_REQUEST')
          done()
        })
    })

    it('it should PUT a manufacturer with id on body', (done) => {
      const jsonData = {
        id: '123e4567-e89b-12d3-a456-42661417400f',
        manufacturerId: '123e4567-e89b-12d3-a456-426614174002',
        model: 'Oakenshield',
        serialNumber: '000001BZ'
      }
      chai.request(server)
        .put('/equipment')
        .send(jsonData)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('OK')
          res.body.body.should.to.be.a('object')
          done()
        })
    })

    it('it should PUT a manufacturer with id on request parameter', (done) => {
      const jsonData = {
        manufacturerId: '123e4567-e89b-12d3-a456-426614174002',
        model: 'Oakenshield',
        serialNumber: '000001BZ'
      }
      chai.request(server)
        .put('/equipment/123e4567-e89b-12d3-a456-42661417400f')
        .send(jsonData)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('OK')
          res.body.body.should.to.be.a('object')
          done()
        })
    })

    it('it should assign an equipment to a manufacturer', (done) => {
      const jsonData = {
        manufacturerId: '123e4567-e89b-12d3-a456-426614174002'
      }
      chai.request(server)
        .put('/equipment/123e4567-e89b-12d3-a456-42661417400f/assignManufacturer')
        .send(jsonData)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('OK')
          res.body.body.should.to.be.a('object')
          done()
        })
    })
  })

  describe('/DELETE equipment', () => {
    it('it should not DELETE an equipment', (done) => {
      chai.request(server)
        .delete('/equipment/123e4567-e89b-12d3-a456-42661417400s')
        .end((err, res) => {
          res.should.have.status(404)
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('NOT_FOUND')
          done()
        })
    })

    it('it should DELETE a equipment', (done) => {
      chai.request(server)
        .delete('/equipment/123e4567-e89b-12d3-a456-42661417400f')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('OK')
          expect(res.body.body).to.be.null
          done()
        })
    })
  })
})
