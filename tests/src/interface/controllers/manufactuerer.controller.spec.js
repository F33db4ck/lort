const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../../../server')
const expect = chai.expect

chai.use(chaiHttp)

describe('Manufacturer', () => {
  let idToRemove = null
  describe('/GET manufacturer', () => {
    it('it should GET all manufacturers', (done) => {
      chai.request(server)
        .get('/manufacturer')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.body.should.be.a('array')
          res.body.type.should.be.equal('OK')
          done()
        })
    })

    it('it should not GET a manufacturer', (done) => {
      chai.request(server)
        .get('/manufacturer/123s4567-e89b-12d3-a456-426614174000')
        .end((err, res) => {
          expect(res.status).to.not.equal(200)
          res.body.should.be.a('object')
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          expect(res.body.body).to.be.null
          done()
        })
    })

    it('it should GET a manufacturer', (done) => {
      chai.request(server)
        .get('/manufacturer/123e4567-e89b-12d3-a456-426614174000')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.body.should.be.a('object')
          res.body.type.should.be.equal('OK')
          done()
        })
    })

    it('it should GET all manufacturers equipments', (done) => {
      chai.request(server)
        .get('/manufacturer/equipment')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.body.should.be.a('array')
          res.body.type.should.be.equal('OK')
          done()
        })
    })

    it('it should GET all equipments of a manufacturer', (done) => {
      chai.request(server)
        .get('/manufacturer/123e4567-e89b-12d3-a456-426614174000/equipment')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.body.should.be.a('array')
          res.body.type.should.be.equal('OK')
          done()
        })
    })
  })

  describe('/POST manufacturer', () => {
    it('it should not POST a manufacturer', (done) => {
      chai.request(server)
        .post('/manufacturer')
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('BAD_REQUEST')
          done()
        })
    })

    it('it should POST a manufacturer with on body id', (done) => {
      const jsonData = {
        id: '123e4567-e89b-12d3-a456-426614174000',
        name: 'Humans'
      }
      chai.request(server)
        .post('/manufacturer')
        .send(jsonData)
        .end((err, res) => {
          res.status.should.be.oneOf([201, 409])
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.oneOf(['CREATED', 'ALREADY_EXISTS'])
          if (res.status === 201) {
            expect(res.body.body).to.be.a('object')
            idToRemove = res.body.body.id
          } else {
            expect(res.body.body).to.be.null
          }
          done()
        })
    })

    it('it should POST a manufacturer without id on body', (done) => {
      const jsonData = {
        name: 'orcs'
      }
      chai.request(server)
        .post('/manufacturer')
        .send(jsonData)
        .end((err, res) => {
          res.status.should.be.oneOf([201, 409])
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.oneOf(['CREATED', 'ALREADY_EXISTS'])
          if (res.status === 201) {
            expect(res.body.body).to.be.a('object')
            idToRemove = res.body.body.id
          } else {
            expect(res.body.body).to.be.null
          }
          done()
        })
    })
  })

  describe('/PUT manufacturer', () => {
    it('it should not PUT a manufacturer', (done) => {
      chai.request(server)
        .put('/manufacturer')
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('BAD_REQUEST')
          done()
        })
    })

    it('it should PUT a manufacturer with id on body', (done) => {
      const jsonData = {
        id: '123e4567-e89b-12d3-a456-426614174000',
        name: 'Humans'
      }
      chai.request(server)
        .put('/manufacturer')
        .send(jsonData)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('OK')
          res.body.body.should.to.be.a('object')
          done()
        })
    })

    it('it should PUT a manufacturer with id on request parameter', (done) => {
      const jsonData = {
        name: 'Humans'
      }
      chai.request(server)
        .put('/manufacturer/123e4567-e89b-12d3-a456-426614174000')
        .send(jsonData)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('OK')
          res.body.body.should.to.be.a('object')
          done()
        })
    })
  })

  describe('/DELETE manufacturer', () => {
    it('it should not DELETE a manufacturer', (done) => {
      chai.request(server)
        .delete('/manufacturer/c1c19570-ace8-4d4a-994e-781826cfd50v')
        .end((err, res) => {
          res.should.have.status(404)
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('NOT_FOUND')
          done()
        })
    })

    it('it should DELETE a manufacturer', (done) => {
      chai.request(server)
        .delete('/manufacturer/' + idToRemove)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.have.property('body')
          res.body.should.have.property('msg')
          res.body.should.have.property('type')
          res.body.type.should.be.equal('OK')
          expect(res.body.body).to.be.null
          done()
        })
    })
  })
})
