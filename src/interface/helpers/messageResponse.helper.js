/**
 * Generates the response msg
 * @param {String} type
 * @param {String} tableName
 * @param {String|null} method
 * @param {String| null} fields
 * @return {string}
 */
module.exports = httpStatusHelper = (type, tableName, method = null, fields = null) => {
  let msg

  if (type === 'CREATED') {
    msg = `${tableName} created!`
  } else if (type === 'OK') {
    const withMsg = fields ? `with ${fields},` : ''
    msg = `${tableName} ${withMsg} ${method}!`
  } else if (type === 'BAD_REQUEST') {
    msg = 'Bad Request'
  } else if (type === 'NOT_FOUND') {
    const withMsg = fields ? `with ${fields}` : ''
    msg = `${tableName} ${withMsg}, not found!`
  } else if (type === 'ALREADY_EXISTS') {
    msg = `${tableName} with ${fields}, already exists!`
  } else {
    msg = 'A internal server error occurred!'
  }

  return msg
}
