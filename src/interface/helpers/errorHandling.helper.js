const messageResponse = require('./messageResponse.helper')
/**
 * Handle caught errors
 * @param {Object} err
 * @param {String|null} tableName
 * @param {String|null} fieldsFormatted
 * @return {{type: string, msg: string, body: null}}
 * */
module.exports = errorHandling = (error, tableName, fieldsFormatted) => {
  let type
  let msg
  console.error(error)
  if (error.message.includes('duplicate key value violates unique constraint')) {
    type = 'ALREADY_EXISTS'
    if (fieldsFormatted) {
      msg = messageResponse(type, tableName, null, fieldsFormatted)
    } else {
      msg = error.message
    }
  } else if (error.message.includes('invalid input syntax for type uuid')) {
    type = 'NOT_FOUND'
    msg = messageResponse(type, tableName, null, fieldsFormatted)
  } else {
    type = 'UNKNOWN'
    msg = error.message
  }

  return {
    type,
    msg,
    body: null
  }
}
