
module.exports = httpStatusHelper = (type) => {
  let statusCode

  if (type === 'CREATED') {
    statusCode = 201
  } else if (type === 'OK') {
    statusCode = 200
  } else if (type === 'BAD_REQUEST') {
    statusCode = 400
  } else if (type === 'NOT_FOUND') {
    statusCode = 404
  } else if (type === 'ALREADY_EXISTS') {
    statusCode = 409
  } else {
    statusCode = 500
  }

  return statusCode
}
