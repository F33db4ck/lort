const createEquipment = require('../../application/usecases/equipments/createEquipment')
const updateEquipment = require('../../application/usecases/equipments/updateEquipment')
const listAllEquipments = require('../../application/usecases/equipments/listAllEquipments')
const listManufacturerOfEquipments = require('../../application/usecases/equipments/listManufacturerOfEquipments')
const findEquipmentManufacturer = require('../../application/usecases/equipments/findEquipmentManufacturer')
const findEquipment = require('../../application/usecases/equipments/findEquipment')
const deleteEquipment = require('../../application/usecases/equipments/deleteEquipment')
const assignManufacturer = require('../../application/usecases/equipments/assignManufacturer')
const httpStatus = require('../helpers/httpStatus.helper')

class EquipmentController {
  createEquipment = async (req, res) => {
    const { id = null, manufacturerId = null, model, serialNumber } = req.body
    const { type, msg, body } = await createEquipment(id, manufacturerId, model, serialNumber)
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  updateEquipment = async (req, res) => {
    let { id = null, manufactureId = null, model, serialNumber } = req.body
    id = id || req.params.id_equipment
    const { type, msg, body } = await updateEquipment(id, manufactureId, model, serialNumber)
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  listAllEquipments = async (req, res) => {
    const { type, msg, body } = await listAllEquipments()
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  findEquipment = async (req, res) => {
    const id = req.params.id_equipment
    const { type, msg, body } = await findEquipment(id)
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  deleteEquipment = async (req, res) => {
    const id = req.params.id_equipment
    const { type, msg, body } = await deleteEquipment(id)
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  listManufactureOfEquipment = async (req, res) => {
    const { type, msg, body } = await listManufacturerOfEquipments()
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  assignManufacturer = async (req, res) => {
    const equipmentId = req.params.id_equipment
    const { manufacturerId = null } = req.body
    const { type, msg, body } = await assignManufacturer(equipmentId, manufacturerId)
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  findEquipmentManufacturer = async (req, res) => {
    const id_equipment = req.params.id_equipment
    const { type, msg, body } = await findEquipmentManufacturer(id_equipment)
    res.status(httpStatus(type)).json({ type, msg, body })
  }
}

module.exports = new EquipmentController()
