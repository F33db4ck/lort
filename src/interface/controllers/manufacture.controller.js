const createManufacturer = require('../../application/usecases/manufacturers/createManufacturer')
const updateManufacturer = require('../../application/usecases/manufacturers/updateManufacturer')
const listAllManufacturers = require('../../application/usecases/manufacturers/listAllManufacturers')
const findManufacturer = require('../../application/usecases/manufacturers/findManufacturer')
const deleteManufacturer = require('../../application/usecases/manufacturers/deleteManufacturer')
const findEquipmentManufacturer = require('../../application/usecases/manufacturers/findEquipmentManufacturer')
const findEquipmentsOfAManufacturer = require('../../application/usecases/manufacturers/findEquipmentsOfAManufacturer')
const httpStatus = require('../helpers/httpStatus.helper')

class ManufactureController {

  createManufacture = async (req, res) => {
    const { id = null, name } = req.body
    const { type, msg, body } = await createManufacturer(id, name)
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  updateManufacturer = async (req, res) => {
    let { name, id: id_manufacturer = null } = req.body
    id_manufacturer = id_manufacturer || req.params.id_manufacturer
    const { type, msg, body } = await updateManufacturer(id_manufacturer, name)
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  listAllManufactures = async (req, res) => {
    const { type, msg, body } = await listAllManufacturers()
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  findManufacturer = async (req, res) => {
    const id_manufacturer = req.params.id_manufacturer
    const { type, msg, body } = await findManufacturer(id_manufacturer)
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  deleteManufacturer = async (req, res) => {
    const id_manufacturer = req.params.id_manufacturer
    const { type, msg, body } = await deleteManufacturer(id_manufacturer)
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  findEquipmentsFromManufacture = async (req, res) => {
    const { type, msg, body } = await findEquipmentManufacturer()
    res.status(httpStatus(type)).json({ type, msg, body })
  }

  findEquipmentsFromAManufacture = async (req, res) => {
    const id_manufacturer = req.params.id_manufacturer
    const { type, msg, body } = await findEquipmentsOfAManufacturer(id_manufacturer)
    res.status(httpStatus(type)).json({ type, msg, body })
  }
}

module.exports = new ManufactureController()
