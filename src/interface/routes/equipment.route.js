const express = require('express')
const { check } = require('express-validator')
const fieldValidator = require('../middlewares/requestFieldsValidation')
const Controller = require('../controllers/equipment.controller')
const router = express.Router()

/**
 * @desc   Create an equipment
 * @route  /equipment/
 * @method POST
 */
router.post('/',
  [
    check('model').not().isEmpty(),
    check('serialNumber').not().isEmpty().isLength({ max: 10 })
  ], fieldValidator, Controller.createEquipment)

/**
 * @desc   Update an equipment
 * @route  /equipment/:id_equipment
 * @method PUT
 */
router.put('/:id_equipment',
  [
    check('model').not().isEmpty(),
    check('serialNumber').not().isEmpty().isLength({ max: 10 })
  ], fieldValidator, Controller.updateEquipment)

/**
 * @desc   Update an equipment
 * @route  /equipment/
 * @method PUT
 */
router.put('/',
  [
    check('id').not().isEmpty(),
    check('model').not().isEmpty(),
    check('serialNumber').not().isEmpty().isLength({ max: 10 })
  ], fieldValidator, Controller.updateEquipment)

/**
 * @desc   List all equipments
 * @route  /equipment/
 * @method GET
 */
router.get('/', Controller.listAllEquipments)

/**
 * @desc   Get equipments with manufacturers
 * @route  /equipment/manufacturer
 * @method GET
 */
router.get('/manufacturer', Controller.listManufactureOfEquipment)

/**
 * @desc   Get Manufacture owner of equipment a specific equipment
 * @route  /equipment/:id_equipment/manufacturer
 * @method GET
 */
router.get('/:id_equipment/manufacturer', Controller.findEquipmentManufacturer)

/**
 * @desc   find an equipment
 * @route  /equipment/:id_equipment
 * @method GET
 */
router.get('/:id_equipment', Controller.findEquipment)

/**
 * @desc   delete an equipment
 * @route  /equipment/:id_equipment
 * @method DELETE
 */
router.delete('/:id_equipment', Controller.deleteEquipment)

/**
 * @desc   Assign a manufacture to equipment
 * @route  /equipment/assignManufacturer
 * @method PUT
 */
router.put('/:id_equipment/assignManufacturer',
  [
    check('manufacturerId').not().isEmpty()
  ], fieldValidator, Controller.assignManufacturer)

module.exports = router
