const express = require('express')
const { check } = require('express-validator')
const fieldValidator = require('../middlewares/requestFieldsValidation')
const Controller = require('../controllers/manufacture.controller')
const router = express.Router()

/**
 * @desc   Create a manufacturer
 * @route /manufacturer/
 * @method POST
 */
router.post('/',
  [check('name').not().isEmpty().isLength({ max: 150 })],
  fieldValidator,
  Controller.createManufacture)

/**
 * @desc   Update a manufacturer
 * @route /manufacturer/
 * @method PUT
 */
router.put('/',
  [
    check('name').not().isEmpty().isLength({ max: 150 }),
    check('id').not().isEmpty()
  ],
  fieldValidator,
  Controller.updateManufacturer)

/**
 * @desc   Update a manufacturer
 * @route  /manufacturer/:id_manufacturer
 * @method PUT
 */
router.put('/:id_manufacturer',
  [check('name').not().isEmpty().isLength({ max: 150 })],
  fieldValidator,
  Controller.updateManufacturer)

/**
 * @desc   list all manufacturers
 * @route  /manufacturer/
 * @method GET
 */
router.get('/', Controller.listAllManufactures)

/**
 * @desc   Find all equipments from the manufacturers
 * @route  /manufacturer/equipment
 * @method GET
 */
router.get('/equipment', Controller.findEquipmentsFromManufacture)

/**
 * @desc   Find equipments that belong to a manufacturer
 * @route  /manufacturer/equipment
 * @method GET
 */
router.get('/:id_manufacturer/equipment', Controller.findEquipmentsFromAManufacture)

/**
 * @desc   Find a manufacturer
 * @route  /manufacturer/:id_manufacturer
 * @method GET
 */
router.get('/:id_manufacturer', Controller.findManufacturer)

/**
 * @desc   Delete a manufacturer
 * @route  /manufacturer/:id_manufacturer
 * @method DELETE
 */
router.delete('/:id_manufacturer', Controller.deleteManufacturer)

module.exports = router
