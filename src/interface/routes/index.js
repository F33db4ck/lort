const express = require('express')

const router = express.Router()

router.use('/', require('./main.route'))
router.use('/manufacturer', require('./manufacture.route'))
router.use('/equipment', require('./equipment.route'))

module.exports = router
