const { validationResult } = require('express-validator')

/*
*
* Validates the fields passed at the body of the request
*
* */
requestFieldsValidation = async (req, res, next) => {
  try {
    const valRes = validationResult(req)
    if (!valRes.isEmpty()) {
      let { msg } = valRes.errors[0]
      msg += ' on ['
      valRes.errors.forEach(errorObj => {
        const { param } = errorObj
        msg += param + ', '
      })
      msg += ']'
      msg = msg.replace(', ]', ']')
      return res.status(400).json({ type: 'BAD_REQUEST', msg })
    }
    next()
  } catch (err) {
    res.status(500).json({ type: 'UNKNOWN', msg: err.toJSON })
  }
}

module.exports = requestFieldsValidation
