require('dotenv').config()
const env = {
  PG_PORT: process.env.PG_PORT,
  PG_USER: process.env.PG_USER,
  PG_HOST: process.env.PG_HOST,
  PG_DATABASE: process.env.PG_DATABASE,
  PG_PSW: process.env.PG_PSW,
  SERVER_PORT: process.env.SERVER_POR
}

module.exports = env
