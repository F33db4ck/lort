const env = require('./env')
const { Pool } = require('pg')

// Connect to the Database
class DbConnection {
  constructor () {
    if (DbConnection._instance) {
      return DbConnection._instance
    }
    DbConnection._instance = this

    this.pool = new Pool({
      user: env.PG_USER,
      host: env.PG_HOST,
      database: env.PG_DATABASE,
      password: env.PG_PSW,
      port: env.PG_PORT
    })

    this.pool.on('connect', () => {
      console.log('Connected to the database!')
    })

    this.pool.on('error', err => {
      console.log('Something Happen trying to connect to database!')
      console.error(err)
    })
  }

  connect = async () => {
    this.pool = await this.pool.connect()
  }

  query = async (text, params) => {
    return await this.pool.query(text, params)
  }

  client = async () => {
    return await this.pool.client()
  }
}

module.exports = new DbConnection()
