class EquipmentsQueries {
  createEquipment = () => {
    return 'INSERT INTO equipment (id, manufacturerid, model, serialnumber) VALUES (COALESCE($1, gen_random_uuid()), $2, $3, $4) RETURNING *'
  }

  findEquipmentByModelAndManufacture = () => {
    return 'SELECT id, manufacturerid, model, serialnumber FROM equipment WHERE model=$1 AND manufacturerid=$2;'
  }

  deleteEquipment = () => {
    return 'DELETE FROM equipment WHERE id=$1;'
  }

  findEquipmentById = () => {
    return `SELECT id, manufacturerid, model, serialnumber 
                FROM equipment 
                WHERE id=$1;
        `
  }

  findEquipmentWithManufacturerById = () => {
    return `SELECT eqp.id, eqp.manufacturerid, man."name" as manufacturer, eqp.model, eqp.serialnumber 
                FROM equipment eqp
                INNER JOIN manufacturer man ON eqp.manufacturerid = man.id
                WHERE eqp.id=$1;
        `
  }

  findAllEquipments = () => {
    return 'SELECT id, manufacturerid, model, serialnumber FROM equipment;'
  }

  findManufacturerOfEquipments = () => {
    return `
            SELECT eqp.id, eqp.manufacturerid, man."name" as manufacturer, eqp.model, eqp.serialnumber 
            FROM equipment eqp 
            INNER JOIN manufacturer man ON eqp.manufacturerid = man.id 
        `
  }

  updateEquipments = () => {
    return 'UPDATE equipment SET manufacturerid=$2, model=$3, serialnumber=$4 WHERE id= $1 RETURNING *;'
  }

  assignManufacturerToEquipment = () => {
    return 'UPDATE equipment SET manufacturerid=$2 WHERE id= $1 RETURNING *;'
  }
}

module.exports = new EquipmentsQueries()
