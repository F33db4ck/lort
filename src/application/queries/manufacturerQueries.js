class ManufacturerQueries {
  createManufacturer = () => {
    return 'INSERT INTO manufacturer(id, "name") VALUES (COALESCE($1, gen_random_uuid()), $2) RETURNING *;'
  }

  deleteAllEquipmentsWithManufacturer = () => {
    return 'DELETE FROM equipment WHERE manufacturerid=$1'
  }

  deleteManufacturer = () => {
    return 'DELETE FROM manufacturer WHERE id=$1;'
  }

  getAllManufacturers = () => {
    return 'SELECT id, "name" FROM manufacturer'
  }

  getManufactureByName = () => {
    return 'SELECT id, "name" FROM manufacturer WHERE "name" like $1'
  }

  getManufactureById = () => {
    return 'SELECT id, "name" FROM manufacturer where id = $1'
  }

  getAllEquipmentsFromManufacturer = () => {
    return `
            SELECT man.id, man."name", eqp.id as equipmentId, eqp.model as equipment from manufacturer man
            INNER JOIN equipment eqp ON man.id = eqp.manufacturerid `
  }

  getAllEquipmentsFromAManufacturer = () => {
    return `
            SELECT man.id, man."name", eqp.id as equipmentId, eqp.model as equipment from manufacturer man
            INNER JOIN equipment eqp ON man.id = eqp.manufacturerid 
            WHERE man.id = $1`
  }

  updateManufacturer = () => {
    return 'UPDATE public.manufacturer SET "name"=$1 WHERE id=$2 RETURNING *'
  }
}

module.exports = new ManufacturerQueries()
