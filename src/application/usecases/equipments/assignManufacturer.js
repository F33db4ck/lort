const EquipmentsQueries = require('../../queries/equipmentsQueries')
const ManufacturerQueries = require('../../queries/manufacturerQueries')
const messageResponse = require('../../../interface/helpers/messageResponse.helper')
const errorHandling = require('../../../interface/helpers/errorHandling.helper')
const db = require('../../../infrastructure/config/db')

/**
 * Assigns a manufacturer to an equipment
 * @param {String} equipmentId
 * @param {String} manufacturerId
 * @param {String|null} method
 * @param {String| null} fields
 * @return {{type: string, msg: string, body: object}}
 */
const assignManufacturer = async (equipmentId, manufacturerId) => {
  /// Check if the equipment with that id exists
  let fields = null
  let tableName = 'equipment'
  try {
    fields = `id: ${equipmentId}`
    const { rowCount: rowCountFind } = await db.query(EquipmentsQueries.findEquipmentById(), [equipmentId])
    if (rowCountFind < 1) {
      const type = 'NOT_FOUND'
      const msg = messageResponse(type, tableName, null, fields)
      return {
        type,
        msg,
        body: null
      }
    } else {
      // Check if the manufacturer exists
      fields = `id: ${manufacturerId}`
      tableName = 'manufacturer'

      const { rowCount: rowCountFind } = await db.query(ManufacturerQueries.getManufactureById(), [manufacturerId])
      if (rowCountFind < 1) {
        const type = 'NOT_FOUND'
        const msg = messageResponse(type, tableName, null, fields)
        return {
          type,
          msg,
          body: null
        }
      } else {
        tableName = 'equipment'
        fields = `equipmentId: ${equipmentId}, manufacturerId: ${manufacturerId}`

        const {
          rowCount: rowCountCreate,
          rows
        } = await db.query(EquipmentsQueries.assignManufacturerToEquipment(), [equipmentId, manufacturerId])
        if (rowCountCreate > 0) {
          const type = 'OK'
          const msg = messageResponse(type, tableName, 'updated', fields)
          return {
            type,
            msg,
            body: rows[0]
          }
        } else {
          throw new Error('Unable to assign a Manufacturer for that Equipment')
        }
      }
    }
  } catch (err) {
    return errorHandling(err, tableName, fields)
  }
}

module.exports = assignManufacturer
