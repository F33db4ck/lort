const EquipmentsQueries = require('../../queries/equipmentsQueries')
const messageResponse = require('../../../interface/helpers/messageResponse.helper')
const errorHandling = require('../../../interface/helpers/errorHandling.helper')
const db = require('../../../infrastructure/config/db')
/**
 * Create new equipment
 * @param {String|null} manufactureId
 * @param {String|null} manufactureId
 * @param {String} model
 * @param {String} serialNumber
 * @return {{type: string, msg: string, body: object}}
 */
module.exports = createEquipment = async (id = null, manufactureId = null, model, serialNumber) => {
  let fields = null
  const tableName = 'equipment'
  try {
    // check if equipment with that model and manufacturer dont exist
    fields = `model: ${model} & manufacturerId: ${manufactureId}`

    const { rowCount: rowCountFind } = await db.query(EquipmentsQueries.findEquipmentByModelAndManufacture(), [model, manufactureId])
    if (rowCountFind > 0) {
      const type = 'ALREADY_EXISTS'
      const msg = messageResponse(type, tableName, null, fields)
      return {
        type,
        msg,
        body: null
      }
    }

    // Create the equipment
    fields = null
    const { rowCount: rowCountCreate, rows } = await db.query(EquipmentsQueries.createEquipment(),
      [id, manufactureId, model, serialNumber])
    if (rowCountCreate > 0) {
      const type = 'CREATED'
      const msg = messageResponse(type, tableName)
      return {
        type,
        msg,
        body: rows[0]
      }
    }

    throw new Error('Something went wrong when trying to create equipment')
  } catch (err) {
    return errorHandling(err, tableName, fields)
  }
}
