const EquipmentsQueries = require('../../queries/equipmentsQueries')
const messageResponse = require('../../../interface/helpers/messageResponse.helper')
const errorHandling = require('../../../interface/helpers/errorHandling.helper')
const db = require('../../../infrastructure/config/db')
/**
 * Updates an equipment
 * @param {String} id
 * @param {String} manufactureId
 * @param {String} model
 * @param {String} serialNumber
 * @return {{type: string, msg: string, body: object}}
 */
module.exports = updateEquipment = async (id, manufactureId, model, serialNumber) => {
  let fields = null
  const tableName = 'equipment'
  try {
    /// Check if the equipment with that id exists
    fields = `id: ${id}`

    const { rowCount: rowCountFind } = await db.query(EquipmentsQueries.findEquipmentById(), [id])
    if (rowCountFind < 1) {
      const type = 'NOT_FOUND'
      const msg = messageResponse(type, tableName, null, fields)
      return {
        type,
        msg,
        body: null
      }
    } else {
      fields = `id: ${id}`

      const { rowCount: rowCountCreate, rows } = await db.query(EquipmentsQueries.updateEquipments(), [id, manufactureId, model, serialNumber])
      if (rowCountCreate > 0) {
        const type = 'OK'
        const msg = messageResponse(type, tableName, 'updated', fields)
        return {
          type,
          msg,
          body: rows[0]
        }
      }
      throw new Error('Something went wrong when trying to update equipment')
    }
  } catch (err) {
    return errorHandling(err, tableName, fields)
  }
}
