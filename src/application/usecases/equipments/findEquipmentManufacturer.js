const EquipmentsQueries = require('../../queries/equipmentsQueries')
const messageResponse = require('../../../interface/helpers/messageResponse.helper')
const errorHandling = require('../../../interface/helpers/errorHandling.helper')
const db = require('../../../infrastructure/config/db')
/**
 * Find equipment by that id
 * @param {String} id
 * @return {{type: string, msg: string, body: object}}
 */
findEquipmentManufacturer = async (id) => {
  let fields = null
  const tableName = 'equipment'
  try {
    // Check if an equipment with that id exists
    fields = `id: ${id}`

    const { rowCount, rows } = await db.query(EquipmentsQueries.findEquipmentWithManufacturerById(), [id])
    if (rowCount < 1) {
      const type = 'NOT_FOUND'
      const msg = messageResponse(type, tableName, null, fields)
      return {
        type,
        msg,
        body: null
      }
    }

    const type = 'OK'
    fields = `id: ${id}`
    const msg = messageResponse(type, tableName, 'found', fields)
    return {
      type,
      msg,
      body: rows[0]
    }
  } catch (err) {
    return errorHandling(err, tableName, fields)
  }
}

module.exports = findEquipmentManufacturer
