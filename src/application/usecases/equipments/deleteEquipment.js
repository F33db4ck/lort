const EquipmentsQueries = require('../../queries/equipmentsQueries')
const messageResponse = require('../../../interface/helpers/messageResponse.helper')
const errorHandling = require('../../../interface/helpers/errorHandling.helper')
const db = require('../../../infrastructure/config/db')
/**
 * Delete manufacture
 * @param {String} id
 * @return {{type: string, msg: string, body: object}}
 */
deleteEquipment = async (id) => {
  let fields = null
  const tableName = 'equipment'
  try {
    // Check if the equipment with that id exists
    fields = `id: ${id}`

    const { rowCount: rowCountFind } = await db.query(EquipmentsQueries.findEquipmentById(), [id])
    if (rowCountFind < 1) {
      const type = 'NOT_FOUND'
      const msg = messageResponse(type, tableName, null, fields)
      return {
        type,
        msg,
        body: null
      }
    } else {
      // Delete the manufacturer
      fields = `id: ${id}`

      const { rowCount: rowCountCreate } = await db.query(EquipmentsQueries.deleteEquipment(), [id])
      if (rowCountCreate > 0) {
        const type = 'OK'
        const msg = messageResponse(type, tableName, 'deleted', fields)
        return {
          type,
          msg,
          body: null
        }
      }
    }
  } catch (err) {
    return errorHandling(err, tableName, fields)
  }
}

module.exports = deleteEquipment
