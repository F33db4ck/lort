const ManufacturerQueries = require('../../queries/manufacturerQueries')
const messageResponse = require('../../../interface/helpers/messageResponse.helper')
const errorHandling = require('../../../interface/helpers/errorHandling.helper')
const db = require('../../../infrastructure/config/db')
/**
 * List equipments of a specific manufacturer
 */
findEquipmentsOfAManufacturer = async (id) => {
  const fields = `id: ${id}`
  const tableName = 'manufacturer'

  try {
    const { rowCount: rowCountFind, rows } = await db.query(ManufacturerQueries.getAllEquipmentsFromAManufacturer(), [id])
    if (rowCountFind < 1) {
      const type = 'NOT_FOUND'
      const msg = messageResponse(type, tableName)
      return {
        type,
        msg,
        body: null
      }
    }
    const type = 'OK'

    const msg = messageResponse(type, tableName, 'found', fields)
    return {
      type,
      msg,
      body: rows
    }
  } catch (err) {
    return errorHandling(err, tableName, fields)
  }
}

module.exports = findEquipmentsOfAManufacturer
