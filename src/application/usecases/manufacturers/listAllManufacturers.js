const ManufacturerQueries = require('../../queries/manufacturerQueries')
const messageResponse = require('../../../interface/helpers/messageResponse.helper')
const errorHandling = require('../../../interface/helpers/errorHandling.helper')
const db = require('../../../infrastructure/config/db')
/**
 * List manufactures
 */
listManufacture = async () => {
  const tableName = 'manufacturer'

  try {
    const { rowCount: rowCountFind, rows } = await db.query(ManufacturerQueries.getAllManufacturers(), null)
    if (rowCountFind < 1) {
      const type = 'NOT_FOUND'
      const msg = messageResponse(type, tableName)
      return {
        type,
        msg,
        body: null
      }
    }
    const type = 'OK'
    const msg = messageResponse(type, tableName, 'found')
    return {
      type,
      msg,
      body: rows
    }
  } catch (err) {
    return errorHandling(err, tableName, null)
  }
}

module.exports = listManufacture
