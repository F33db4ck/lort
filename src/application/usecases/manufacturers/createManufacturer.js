const ManufacturerQueries = require('../../queries/manufacturerQueries')
const messageResponse = require('../../../interface/helpers/messageResponse.helper')
const errorHandling = require('../../../interface/helpers/errorHandling.helper')
const db = require('../../../infrastructure/config/db')
/**
 * Create a new manufacture
 * @param {String|null} id
 * @param {String} name
 * @return {{type: string, msg: string, body: object}}
 */
createManufacture = async (id = null, name) => {
  let fields = null
  const tableName = 'manufacturer'
  try {
    // Check if a manufacturer with that name doesnt exist
    fields = `name: ${name}`

    const { rowCount: rowCountFind } = await db.query(ManufacturerQueries.getManufactureByName(), [name])
    if (rowCountFind > 0) {
      const type = 'ALREADY_EXISTS'

      const msg = messageResponse(type, tableName, 'found', fields)
      return {
        type,
        msg,
        body: null
      }
    }

    // Create the manufacturer
    fields = null
    const { rowCount: rowCountCreate, rows } = await db.query(ManufacturerQueries.createManufacturer(), [id, name])
    if (rowCountCreate > 0) {
      const type = 'CREATED'
      const msg = messageResponse(type, tableName)
      return {
        type,
        msg,
        body: rows[0]
      }
    }

    throw new Error('Something went wrong when trying to create a manufacturer')
  } catch (err) {
    return errorHandling(err, tableName, fields)
  }
}

module.exports = createManufacture
