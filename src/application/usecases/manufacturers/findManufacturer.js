const ManufacturerQueries = require('../../queries/manufacturerQueries')
const messageResponse = require('../../../interface/helpers/messageResponse.helper')
const errorHandling = require('../../../interface/helpers/errorHandling.helper')
const db = require('../../../infrastructure/config/db')
/**
 * Create new manufacture
 * @param {String} id
 * @return {{type: string, msg: string, body: object}}
 */
findManufacture = async (id) => {
  const fields = `id: ${id}`
  const tableName = 'manufacturer'

  try {
    // Check if a manufacturer with that name doesnt exist
    const { rowCount, rows } = await db.query(ManufacturerQueries.getManufactureById(), [id])
    if (rowCount < 1) {
      const type = 'NOT_FOUND'

      const msg = messageResponse(type, tableName, null, fields)
      return {
        type,
        msg,
        body: null
      }
    }

    const type = 'OK'
    const msg = messageResponse(type, tableName, 'found', fields)
    return {
      type,
      msg,
      body: rows[0]
    }
  } catch (err) {
    return errorHandling(err, tableName, fields)
  }
}

module.exports = findManufacture
