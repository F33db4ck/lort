const ManufacturerQueries = require('../../queries/manufacturerQueries')
const messageResponse = require('../../../interface/helpers/messageResponse.helper')
const errorHandling = require('../../../interface/helpers/errorHandling.helper')
const client = require('../../../infrastructure/config/db')
/**
 * Delete manufacture
 * @param {String} id
 * @return {{type: string, msg: string, body: object}}
 */
deleteManufacture = async (id) => {
  let fields = null
  const tableName = 'manufacturer'
  try {
    await client.query('BEGIN')
    fields = `id: ${id}`
    const { rowCount: rowCountFind } = await client.query(ManufacturerQueries.getManufactureById(), [id])
    await client.query('COMMIT')
    if (rowCountFind < 1) {
      const type = 'NOT_FOUND'
      const msg = messageResponse(type, tableName, null, fields)
      return {
        type,
        msg,
        body: null
      }
    } else {
      fields = `id: ${id}`
      await client.query(ManufacturerQueries.deleteAllEquipmentsWithManufacturer(), [id])
      await client.query(ManufacturerQueries.deleteManufacturer(), [id])
      await client.query('COMMIT')
      const type = 'OK'
      const msg = messageResponse(type, tableName, 'deleted', fields)
      return {
        type,
        msg,
        body: null
      }
    }
  } catch (err) {
    await client.query('COMMIT')
    return errorHandling(err, tableName, fields)
  }
}

module.exports = deleteManufacture
