const ManufacturerQueries = require('../../queries/manufacturerQueries')
const messageResponse = require('../../../interface/helpers/messageResponse.helper')
const errorHandling = require('../../../interface/helpers/errorHandling.helper')
const db = require('../../../infrastructure/config/db')
/**
 * Update manufacture
 * @param {String} id
 * @param {String} name
 * @return {{type: string, msg: string, body: object}}
 */
updateManufacture = async (id, name) => {
  const tableName = 'manufacturer'
  let fields = `id: ${id}`
  try {
    // Check if a manufacturer with that name doesnt exist
    const { rowCount: rowCountFind } = await db.query(ManufacturerQueries.getManufactureById(), [id])
    if (rowCountFind < 1) {
      const type = 'NOT_FOUND'
      const msg = messageResponse(type, tableName, null, fields)
      return {
        type,
        msg,
        body: null
      }
    } else {
      // Update the manufacturer
      fields = `id: ${id}, name: ${name}`

      const { rowCount: rowCountCreate, rows } = await db.query(ManufacturerQueries.updateManufacturer(), [name, id])
      if (rowCountCreate > 0) {
        const type = 'OK'
        const msg = messageResponse(type, tableName, 'updated', fields)
        return {
          type,
          msg,
          body: rows[0]
        }
      }
    }
  } catch (err) {
    return errorHandling(err, tableName, fields)
  }
}

module.exports = updateManufacture
