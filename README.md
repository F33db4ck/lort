# NodeJS Backend Developer - Exercise #

## About The Exercise
This is just a simple summary of the exercise you can find the complete description of the
exercise [here](https://drive.google.com/file/d/1fmHal4oOiWiY9lIJtxuoe6Vjc7NxSspF/view?usp=sharing) 
This exercise is to create a NodeJS REST application automatically deployed using BitBucket 
Pipelines on a free-tier AWS EC2 server. 

Deploy the application on an AWS EC2 linux cloud 
server which will provide access to the following REST endpoints:

**For manufacturer:**
   
   ```http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer```

   - Create or update a manufacturer, list all manufacturers
  
   ```http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer/{ID}```

   - Read or Delete a manufacturer

   ```http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer/equipment```

   - List all equipments of the manufacturer

**For equipments:**

   ```http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment```

   - Create or update an equipment, list all equipments
   
   ```http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment/{ID}```

   - Read or Delete an equipment
   
   ```http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment/manufacturer```

   - Get the manufacturer owner of this equipment

where Manufacturer and Equipment are entities taken from a local PostgreSQL database.

**The Manufacturer Entity** will have an id of type UUID and a name of type varchar.

**The Equipment Entity** will have an id of type UUID, a model of type varchar and a serial number of type varchar.

The relationship between **Manufacturer** and **Equipment** is one-to-many. 
One Manufacturer can have several Equipment, but one Equipment can be assigned only to a single Manufacturer.

<br />

## Built With
* [Javascript]()
* [Node.js](https://nodejs.org/)
* [Express](https://expressjs.com/)
* [PG library for PostgreSQL](https://node-postgres.com/)

<br />

## The required operations for the REST API ###

1. Your REST endpoints must be able to support CREATE, READ, UPDATE, DELETE. 
   There is no need for you to implement SEARCH endpoints, this will not be evaluated.
   

2. Your Client REST endpoint must support the ability to create, modify and delete a relationship to an already existing Equipment.


3. Please DO NOT USE ANY ORM framework (e.g. TypeORM/Prism/etc). You must use only the nodejs pg library to integrate PostgreSQL to your NodeJS application.


### The tests that you must perform to deliver ####
The README file must contain instructions with CURL to verify that:

1. A manufacturer can be created

2. A product can be created
   
3. A relationship between one manufacturer and product can be created

4. If a manufacturer is deleted using the REST endpoint, any related equipment will be also automatically deleted.

5. We will use JSON for the REST endpoints. You must document all JSON data we will need to test

<br />

## Available Endpoints
   * ```[GET] "http://<your-aws-ec2-server-ip>:<your-preferred-port>/ ``` - To know if the app is up end running

  **Manufacturer**

   * ```[POST]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer ``` - To create a manufacturer
     

   * ```[PUT]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer ``` - To update a new manufacturer
     

   * ```[GET]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer/{id_manufacturer} ``` - To update a new manufacturer
   

   * ```[GET]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer ``` - To list all manufacturer
     

   * ```[GET]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer/equipment ``` - To list all equipments from the manufacturers
     

   * ```[GET]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer/{id_manufacturer}/equipment ``` - To list equipments that belong to a manufacturer
     

   * ```[GET]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer/{id_manufacturer} ``` - To find a manufacturer
     

   * ```[DELETE]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer/{id_manufacturer}``` - To delete a manufacturer
     
**Equipment**

   * ```[POST]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment ``` - To create an equipment
     

   * ```[PUT]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/{id_equipment} ``` - To update an equipment
     

   * ```[PUT]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment ``` - To update an equipment
     

   * ```[GET]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment ``` - To list all equipments
     

   * ```[GET]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment/manufacturer ``` - To get manufacture owner of equipments
     

   * ```[GET]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment/{id_equipment}/manufacturer ``` - To get manufacture owner of equipment a specific equipment
     

   * ```[GET]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment/{id_equipment} ``` - To find an equipment
     

   * ```[DELETE]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment/{id_equipment} ``` - To delete an equipment
     

   * ```[PUT]  "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment.{id_equipment}/assignManufacturer ``` - To assign a manufacture to equipment


## CURL & JSON data used to test the REST endpoints ##
   
### Returned Data ###

   The returned data its always an object with the following structure:
   ```json
      {
         "type": "String",
         "msg": "String",
         "body": "Object"
      }
   ``` 

   example:
```json
    {
        "type":"OK",
        "msg":"manufacturer with id: 123e4567-e89b-12d3-a456-426614174000, updated!",
        "body":{  "id":"123e4567-e89b-12d3-a456-426614174000", "name":"Humans"}
    }
```

### JSON data explanation ###

   For **Manufacturer**:
   - ```id``` - A valid UUID 
   - ```name``` - Name of the manufacturer 

   For **Equipment**:
   - ```id``` - A valid UUID
   - ```manufacturerId``` - A valid UUID for a manufacturer (optional)
   - ```model``` - A name of the model
   - ```serialNumber``` - A serial number of the model

   <br />

#### 0. The curl command to Check if the API is up and running ####
   
###### cURL command ######
   ```
   curl -H "Content-Type: application/json" -X GET "http://<your-aws-ec2-server-ip>:<your-preferred-port>/"
   ```
   
   <br />

#### 1. The curl commands with its respective json’s to create the manufacturers at manufacturer section ####

###### JSON ######

   Create manufacturer with UUID

   ```json
   {
      "id": "String",
      "name": "String" 
   }
   ```
   
   Create a manufacturer with a generated UUID

   ```json
   {
      "name": "String" 
   }
   ```

###### cURL commands ######

   ```sh
   curl -d '{"id": "123e4567-e89b-12d3-a456-426614174000", "name": "Humans"}' -H "Content-Type: application/json" -X POST "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer"
   
   curl -d '{"id": "123e4567-e89b-12d3-a456-426614174001", "name": "Elves"}' -H "Content-Type: application/json" -X POST "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer"
   
   curl -d '{"id": "123e4567-e89b-12d3-a456-426614174002", "name": "Dwarves"}' -H "Content-Type: application/json" -X POST "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer"
   
   curl -d '{"name": "Orcs"}' -H "Content-Type: application/json" -X POST "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer"
   ```

   <br />

#### 2. The curl commands with its respective json’s to create all equipment at the equipments section.

###### JSON ######
   
   Create an equipment with an UUID   

   ```json
   {
      "id": "String",
      "manufacturerId": "String",
      "model": "String",
      "serialNumber": "String"
   }
   ```

   Create an equipment with a generated UUID   

   ```json
   {
      "manufacturerId": "String",
      "model": "String",
      "serialNumber": "String"
   }
   ```
   
   Create an equipment with a generated UUID and without a manufacturer

   ```json
   {
      "model": "String",
      "serialNumber": "String"
   }
   ```

###### cURL commands ######
   ```sh
   curl -d '{"id": "123e4567-e89b-12d3-a456-42661417400a", "manufacturerId": "123e4567-e89b-12d3-a456-426614174000", "model": "The Anduril Sword", "serialNumber": "000001A"}' -H "Content-Type: application/json" -X POST "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment"

   curl -d '{"id": "123e4567-e89b-12d3-a456-42661417400b", "manufacturerId": "123e4567-e89b-12d3-a456-426614174001", "model": "Long Battle Bow", "serialNumber": "000001A"}' -H "Content-Type: application/json" -X POST "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment"
   
   curl -d '{"id": "123e4567-e89b-12d3-a456-42661417400c", "manufacturerId": "123e4567-e89b-12d3-a456-426614174002", "model": "Golden Helm", "serialNumber": "000001B"}' -H "Content-Type: application/json" -X POST "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment"
   
   curl -d '{"id": "123e4567-e89b-12d3-a456-42661417401c",  "model": "Sacred Ring", "serialNumber": "000001B"}' -H "Content-Type: application/json" -X POST "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment"
   
   curl -d '{"id": "123e4567-e89b-12d3-a456-42661417400d", "manufacturerId": "123e4567-e89b-12d3-a456-426614174000", "model": "Shield of Gondor", "serialNumber": "000001B1"}' -H "Content-Type: application/json" -X POST "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment"
   
   curl -d '{"id": "123e4567-e89b-12d3-a456-42661417400e", "manufacturerId": "123e4567-e89b-12d3-a456-426614174000", "model": "Leather Breastplate", "serialNumber": "000001C"}' -H "Content-Type: application/json" -X POST "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment"
   
   curl -d '{"model": "Orcrist", "serialNumber": "000001D"}' -H "Content-Type: application/json" -X POST "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment"
   ```  

   <br />
   
### 3. Curl commands with its json to update one manufacturer and one equipment.

#### Update one manufacturer 

###### JSON ######
   Update manufacturer passing the id of the manufacturer on the body
   ```json
   {
      "id": "String",
      "name": "String" 
   }
   ```

   updated manufacturer passing the id of the manufacturer as query parameter
   ```json
   {
      "name": "String" 
   }
   ```
###### cURL examples ######

   ```sh
   curl -d '{"id": "123e4567-e89b-12d3-a456-426614174000", "name": "Humans"}' -H "Content-Type: application/json" -X PUT "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer"
   
   curl -d '{"name": "Elves"}' -H "Content-Type: application/json" -X PUT "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer/123e4567-e89b-12d3-a456-426614174001"
   ```

#### Update one Equipment ####

###### JSON ######
   
   Update equipment passing the id of the equipment on the body
   ```json
   {
      "id": "String",
      "manufacturerId": "String",
      "model": "String",
      "serialNumber": "String"
   }
   ```

   Updated equipment passing the id of the equipment as query parameter
   ```json
   {
      "manufacturerId": "String",
      "model": "String",
      "serialNumber": "String"
   }
   ```

###### cURL examples ###### 
   ```sh
   curl -d '{"id": "123e4567-e89b-12d3-a456-42661417400d", "manufacturerId": "123e4567-e89b-12d3-a456-426614174000", "model": "Shield of Gondor", "serialNumber": "000001B1"}' -H "Content-Type: application/json" -X PUT "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment"
   
   curl -d '{"manufacturerId": "123e4567-e89b-12d3-a456-426614174000", "model": "Leather Breastplate", "serialNumber": "000001C"}' -H "Content-Type: application/json" -X PUT "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment/123e4567-e89b-12d3-a456-42661417400e"
   
   curl -d '{"model": "Orcrist", "serialNumber": "000001D"}' -H "Content-Type: application/json" -X PUT "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment/123e4567-e89b-12d3-a456-42661417400e"
   ```

   <br />

### 4. Curl commands to delete a manufacturer, delete an equipment
   
   On these commands,we will pass the id of the manufacturer or equipment we want to remove as a query
   parameter to the URL

#### Manufacturer cURL example ####
   
   ```sh
   curl -X DELETE "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer/123e4567-e89b-12d3-a456-426614174001"
   ```

#### Equipment cURL example ####

   ```sh
   curl -X DELETE "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment/123e4567-e89b-12d3-a456-42661417400c" 
   ```

   <br />

### 5. Curl commands to get the manufacturer owner of an equipment ###

   On these commands, we will pass the id of the manufacturer or equipment we want to remove as a query
   parameter to the URL

###### Manufacturer cURL example ######

   ```sh
   curl -X GET "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment/123e4567-e89b-12d3-a456-426614174002"
   ```

###### Equipment cURL example ######

   ```sh
   curl -X GET "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment/123e4567-e89b-12d3-a456-42661417400c"
   ```

   <br />

### 6. Curl commands to get the equipments of a manufacturer ###
 
###### cURL examples ######

   ```sh
   curl -X GET "http://<your-aws-ec2-server-ip>:<your-preferred-port>/manufacturer/equipment"
   ```

   <br />

### 7. Curl commands to assign a manufacturer to an equipment ###

```json
{
   "manufacturerId": "String"
}
```
###### cURL examples ######

   ```sh
   curl -X PUT "http://<your-aws-ec2-server-ip>:<your-preferred-port>/equipment.{id_equipment}/assignManufacturer "
   ```

   <br />


## Credentials ##
   The app can be found at ```/home/app/lort``` on the server

### Credentials to SSH access the linux-based cloud server ###
   ip - ```<your-aws-ec2-server-ip>```

   username - ```USERNAME```

   password - ```PASSWORD```

   command - ```ssh username@ip```
   
   <br />

### Credentials to access the database ###

   **NOTE:** Im using a dockerized version o **PostgreSQL**, that's running on the cloud server
   
   user - ```sa```

   password - ```pws123456```

   hostname - ```http://<your-aws-ec2-server-ip>```

   port - ```5432```

   database - ```lotrdb```
   
<br />

## pm2 commands ##
   
#### How to access the log data from your application ####

   - error log path => /root/.pm2/logs/server-error.log 
     
   - output log path => /root/.pm2/logs/server-out.log  
   
   
#### Instructions to stop, restart and start the application using pm2 ####
   
   ```sh
    # Navigate to the app directory
    cd /home/app/lort
    
    # To start
    sudo pm2 start server.js ;
    
    # Once the app is running you can stop it from anywhere with command
    sudo pm2 stop server
    
    # Or Restart with the command
    sudo pm2 restart server
   
   ```

   There are no manual operations to be executed, operations are executed automatically.

<br />

## Installation ##
   1. Install [node.js](https://nodejs.org/)

   2. Clone the repo
```sh
git clone git@bitbucket.org:F33db4ck/lort.git
```
   
   3. Install the packages
```sh
npm install
```
   
   
   4. Create a .env file with the fields bellow and replace them with the proper information:
   ```
   PG_USER=$PG_USER
   PG_HOST=$PG_HOST
   PG_DATABASE=$PG_DATABASE
   PG_PSW=$PG_PSW
   PG_PORT=$PG_PORT
   SERVER_PORT=$SERVER_PORT
   ```
   
   5. Run ```node server.js``` just like that the server is up and running if the file where configured correctly.
   
   
   Then you are good to go.
   
   

