const express = require('express')
const dbConnection = require('./src/infrastructure/config/db')
const env = require('./src/infrastructure/config/env')

const app = express()
const PORT = env.SERVER_PORT || 5000

app.use(express.json({ extended: false }))
app.use('/', require('./src/interface/routes/index'))
dbConnection.connect().then(() => {
  app.listen(PORT, () => console.log(`Server Started on port ${PORT}`))
})

module.exports = app // for testing
